### Step 1. Starting the Rescue System - Linux 64bit
 Activating the Rescue System

To start a server in the Rescue System, it needs to be activated in the Robot.

Under "Main Functions; Server" select the desired server and then open the tab "Rescue". Here the desired variant can be activated.

The password that was given to you when you activated the Rescue System can now be used to login as "root" via SSH.
Restarting the Server

To load the Rescue System, the server must be restarted.

If you no longer have access to the server, you can use the reset function in the Robot. You will find this under the "Reset" tab of the desired server.

Please note that the activation of the Rescue System is only valid for one boot. If you want to boot your server to the Rescue System again, you will have to activate it in the Hetzner Robot again. If you do not reboot your server within 60 minutes after the activation, the scheduled boot of the Rescue System will automatically become inactive. If the server is restarted later, the system will boot from the internal drive(s). 

### Step 2. Connect to Rescue System vis SSH

### Step 3. Gather Network Inforation
We need to find `real interface name`, `IP address`, `mask` and `gateway`

 - Real Interface Name
```
udevadm info -e | grep -A 20 ^P.*eth0 | grep ID_NET_NAME_PATH
```
Example of output: `E: ID_NET_NAME_PATH=enp35s0`
My interface is `enp35s0`

- In some cases, you will need to check the kernel logs to find real name:
```
cat /var/log/kernel | grep renamed
```

 - IP Address/Netmask & Gateway
 ```
 ip -c a
 ip route
 ```
 
### Step 4. Start QEMU Emulator for installation the OS to disks
 - Dowload Proxmox VE latest ISO Image
 Go to http://download.proxmox.com/iso/ and copy download link of latest proxmox-ve ISO
 ```
 wget 'http://download.proxmox.com/iso/proxmox-ve_6.2-1.iso' -O proxmox-ve_6.0-1.iso
 ```
 ```
 lsblk
 printf "change vnc password\n%s\n" MYPASSWORD | qemu-system-x86_64 -enable-kvm -m 10240 -hda /dev/nvme0n1 -hdb /dev/nvme1n1 -cdrom proxmox-ve_6.0-1.iso -boot d -vnc :0,password -monitor stdio
 ```
 `MYPASSWORD` is a password for VNC connection
 
### Step 5. Connect via VNC to your server port 5900, and follow the installation procedure
Use `serverip:0` and your password  
During install properly configure the Network with previously found information.
- Turn off `qemu` at install finish  
- Then run qemu without `cdrom`:
```
printf "change vnc password\n%s\n" MYPASSWORD | qemu-system-x86_64 -enable-kvm -m 10240 -hda /dev/nvme0n1 -hdb /dev/nvme1n1 -vnc :0,password -monitor stdio
```
Qemu will boot to installed Proxmox.

Edit network interfaces by changing `interface name`:
```
nano /etc/network/interfaces
```

```
auto lo
iface lo inet loopback

iface enp35s0 inet manual

auto vmbr0
iface vmbr0 inet static
    address 117.xxx.xxx.108
    netmask 255.255.255.192
    gateway 117.yyy.yyy.65
    bridge_ports enp35s0
    bridge_stp off
    bridge_fd 0
```

- Poweroff Proxmox, close qemu & and restart dedicated server

### Step 6. Connect to Web interface
 - Connect to https://serverip:8006
 - Enjoy